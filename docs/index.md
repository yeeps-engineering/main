# Intro


## Genral terms
- Diagram - using a key (standard seen bellow) there will be a diagram below, there will also be a perspective reference such as "Birds-Eye"
- B = Bomb
- N = Normal block, could be a 1x1 or bigger
- I = indestructible block, could be 1x1 or bigger

More could be added to each diagram

## Methods of gates
Note - for new method to be added, it should be able to be converted to the list bellow (like how a player can hit a bomb, and a bomb can bush a player.)

- ### [Explosive (Yellow) Bombs](#explosive-bombs)
Explosive bombs will break any blocks in a five block radius, which can detonate another bomb, creating a chain

- ### [Players](#players)
Using players we can throw them around, using pearls, launch pads, and exploding them precisely


## Explosive Bombs
Using yellow bombs we can chain them together to make wiring.
Each bombs has a radius of 5, So looking at this diagram (x = exploded block) we can se how far a bomb may reach:
NNNxxxxBxxxxNNN

## Players
Using players we can throw them around, using pearls, launch pads, and exploding them precisely
